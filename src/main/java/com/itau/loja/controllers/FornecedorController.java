package com.itau.loja.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.loja.models.Fornecedor;
import com.itau.loja.repositories.FornecedorRepository;

@RestController
public class FornecedorController {

	@Autowired//injeção de dependencia
	FornecedorRepository fornecedorRepository;
	
	@RequestMapping(path="/fornecedor")
	public Fornecedor criarFornecedor(@Valid @RequestBody Fornecedor fornecedor){
		return fornecedorRepository.save(fornecedor);
	}
}
