package com.itau.loja.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.loja.models.Produto;
import com.itau.loja.repositories.ProdutoRepository;

@RestController
public class ProdutoController {
	
	@Autowired
	ProdutoRepository produtoRepository;

	@RequestMapping(method=RequestMethod.POST, path="/produto")
	public Produto criarProduto(@Valid @RequestBody Produto produto) {
		return produtoRepository.save(produto);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/produtos")
	public Iterable<Produto> buscarProdutos() {
		return produtoRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/produto/{id}")
	public ResponseEntity<?> buscarProdutos(@PathVariable long id) {
		Optional<Produto> optionalProduto = produtoRepository.findById(id);
		
		if(!optionalProduto.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(optionalProduto.get());
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/produto/{id}")
	public ResponseEntity<?> alterarProduto(@PathVariable long id, @Valid @RequestBody Produto produto) {
		Optional<Produto> optionalProduto = produtoRepository.findById(id);
		
		if(!optionalProduto.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Produto produtoDoBanco = optionalProduto.get();
		
		produtoDoBanco.setNome(produto.getNome());
		produtoDoBanco.setPreco(produto.getPreco());
		
		Produto produtoSalvo = produtoRepository.save(produtoDoBanco);
		
		return ResponseEntity.ok().body(produtoSalvo);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/produto/{id}")
	public ResponseEntity<?> deletarProduto(@PathVariable long id) {
		Optional<Produto> optionalProduto = produtoRepository.findById(id);
		
		if(!optionalProduto.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		produtoRepository.deleteById(id);
		
		return ResponseEntity.ok().build();
	}
}
