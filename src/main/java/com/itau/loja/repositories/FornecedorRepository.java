package com.itau.loja.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.loja.models.Fornecedor;

public interface FornecedorRepository extends CrudRepository<Fornecedor, Long>{

}
